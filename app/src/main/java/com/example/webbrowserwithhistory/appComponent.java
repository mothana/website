package com.example.webbrowserwithhistory;

import com.example.webbrowserwithhistory.browser.BrowserActivity;
import com.example.webbrowserwithhistory.history.ui.history.HistoryFragment;

import javax.inject.Singleton;

import dagger.Component;
@Singleton
@Component(modules = {RepoModule.class,ViewModelModule.class})
public interface appComponent {
    void inject(BrowserActivity activity);
    void inject(HistoryFragment fragment);
}
