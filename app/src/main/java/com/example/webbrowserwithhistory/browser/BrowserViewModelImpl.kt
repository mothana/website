package com.example.webbrowserwithhistory.browser

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.webbrowserwithhistory.common.Repo
import com.example.webbrowserwithhistory.common.Website
import java.util.*
import javax.inject.Inject
// I have set this viewModel to use the Interface/Impl approach. The other one is just a straight concrete class. this mostly to show the differences between them.
class BrowserViewModelImpl @Inject constructor(var repo: Repo) : BrowserViewModel, ViewModel() {

    private val urlLiveData = MutableLiveData<String>()
    override fun updateUrl(url: String) {
        urlLiveData.value = formatUrlText(url)
    }

    override fun getUrl(): LiveData<String> {
        return urlLiveData
    }

    /*
    * Basic check for url inputs. Following the Google chrome style.  Wont work for spelling mistakes in the preface of the address.
    */
    private fun formatUrlText(url: String) =
        when (url.startsWith("http://") || url.startsWith("https://")) {
            true -> url
            false -> "https://$url"
        }

    override fun saveBitmap(uri: Uri, url: String, currentTimeMillis: Long) {
        repo.saveWebsite(Website(url, Date(currentTimeMillis), uri))
    }


}