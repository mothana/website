package com.example.webbrowserwithhistory.browser

import android.net.Uri
import androidx.lifecycle.LiveData

interface BrowserViewModel {
    fun updateUrl(url: String)
    fun saveBitmap(uri: Uri, url: String, currentTimeMillis: Long)
    fun getUrl():LiveData<String>
}