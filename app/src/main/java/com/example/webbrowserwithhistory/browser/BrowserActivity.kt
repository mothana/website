package com.example.webbrowserwithhistory.browser

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.webbrowserwithhistory.MyApplication
import com.example.webbrowserwithhistory.R
import com.example.webbrowserwithhistory.common.Website
import com.example.webbrowserwithhistory.history.HistoryActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject


class BrowserActivity : AppCompatActivity(R.layout.activity_main) {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val MY_PERMISSIONS_REQUEST_READ_STORAGE: Int = 1
    private lateinit var model: BrowserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ButterKnife.bind(this)

        (application as MyApplication).getAppcomponent().inject(this)
        model = ViewModelProviders.of(this, viewModelFactory).get(BrowserViewModelImpl::class.java)


        webview.settings.javaScriptEnabled = true
        webview.webViewClient = CustomWebClient()
        model.getUrl().observe(this, Observer {
            webview.loadUrl(it)
        })
    }

    @OnClick(R.id.go_button)
    fun updateUrl() {
        model.updateUrl(url_edit_text.text.toString())
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("website")) {
            val website = intent.getParcelableExtra<Website>("website")
            intent.removeExtra("website")
            webviewOverlay.setImageURI(website.uri)
            model.updateUrl(website.url)
        }
    }

    @OnClick(R.id.history_button)
    fun goToHistory() {
        startActivity(Intent(this, HistoryActivity::class.java))
    }

    @OnClick(R.id.capture_button)
    fun saveWebsite() {

        if (isExternalStorageWritable() && havePermission()) {
            val bitmap = Bitmap.createBitmap(
                webviewFrame.width,
                webviewFrame.height, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            webviewFrame.draw(canvas)
            val fos: FileOutputStream?
            try {
                val folderDir =
                    File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "screenshots")
                Log.d("makeDir", filesDir.path)

                folderDir.mkdirs()

                val file = File(folderDir, "${System.currentTimeMillis()}.jpeg")
                fos = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.close()
                val uri = Uri.fromFile(file)
                model.saveBitmap(uri, webview.url, System.currentTimeMillis())

            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(
                    applicationContext,
                    "Ahh, something went wrong saving the image",
                    Toast.LENGTH_LONG
                ).show()
            }
        }


    }

    private fun havePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                MY_PERMISSIONS_REQUEST_READ_STORAGE
            )
        } else {
            return true
        }
        return false
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    saveWebsite()
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Write Permissions are need to save websites",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    private fun isExternalStorageWritable() =
        Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED


    inner class CustomWebClient : WebViewClient() {
        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            capture_button.isEnabled = false
            webview.visibility = View.GONE
            webviewOverlay.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            capture_button.isEnabled = true
            webviewOverlay.visibility = View.GONE
            webview.visibility = View.VISIBLE
            webviewOverlay.setImageBitmap(null)
        }

        override fun onReceivedError(
            view: WebView,
            request: WebResourceRequest,
            error: WebResourceError
        ) {
            super.onReceivedError(view, request, error)
            Toast.makeText(applicationContext, "Cannot load page", Toast.LENGTH_SHORT).show()
        }
    }
}
