package com.example.webbrowserwithhistory.common

import javax.inject.Inject
class RepoImpl @Inject constructor() : Repo {
    val list = ArrayList<Website>()

    override fun getWebsites(): List<Website> {
        return list
    }

    override fun saveWebsite(website: Website) {
        list.add(website)
    }

    override fun removeWebsite(website: Website) {
        list.remove(website)
    }
}