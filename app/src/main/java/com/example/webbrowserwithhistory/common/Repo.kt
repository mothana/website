package com.example.webbrowserwithhistory.common

public interface Repo{
    fun getWebsites() :List<Website>
    fun saveWebsite(website: Website)
    fun removeWebsite(website: Website)
}