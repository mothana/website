package com.example.webbrowserwithhistory.common

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
@Parcelize
 data class Website(val url:String, val dateTime:Date, val uri: Uri) : Parcelable
