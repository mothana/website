package com.example.webbrowserwithhistory.history.ui.history

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.webbrowserwithhistory.MyApplication
import com.example.webbrowserwithhistory.R
import com.example.webbrowserwithhistory.browser.BrowserActivity
import com.example.webbrowserwithhistory.common.Website
import kotlinx.android.synthetic.main.history_fragment.*
import javax.inject.Inject


class HistoryFragment : Fragment(R.layout.history_fragment) {
    companion object {
        fun newInstance() = HistoryFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: HistoryViewModel

    private lateinit var adapter: HistoryAdapter
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity?.application as MyApplication).getAppcomponent().inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel::class.java)
        val actionListener = object : HistoryAdapter.ActionsListener {
            override fun deleteWebsite(website: Website)=viewModel.deleteWebsite(website)
            override fun loadWebsite(website: Website) {
                val intent = Intent(context, BrowserActivity::class.java).apply {
                    putExtra("website", website)
                }
                startActivity(intent)
            }
        }
        adapter = HistoryAdapter(actionListener)
        viewModel.list.observe(this, Observer {
            adapter.setItems(it)

        })

        recylerView.adapter = adapter
        recylerView.layoutManager = LinearLayoutManager(context)

    }

}
