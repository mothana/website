package com.example.webbrowserwithhistory.history.ui.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.webbrowserwithhistory.R
import com.example.webbrowserwithhistory.common.Website

class HistoryAdapter(
    private var actionListener: ActionsListener
) :
    Adapter<HistoryAdapter.WebsiteViewHolder>() {

    private var historyItems = ArrayList<Website>()

    fun setItems(items: List<Website>) {
        historyItems = ArrayList(items)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = WebsiteViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.recycler_view_item, parent, false
        )
    )

    override fun getItemCount() = historyItems.size

    override fun onBindViewHolder(holder: WebsiteViewHolder, position: Int) {
        holder.bind(historyItems[position])
        holder.setOnClickListnerss(
            View.OnClickListener {actionListener.deleteWebsite(historyItems[position]) },
            View.OnClickListener { actionListener.loadWebsite(historyItems[position]) }) }

    interface ActionsListener {
        fun deleteWebsite(website: Website)
        fun loadWebsite(website: Website)
    }


    class WebsiteViewHolder(itemView: View) : ViewHolder(itemView) {

        val url: TextView? = itemView.findViewById(R.id.list_url)
        val image: ImageView? = itemView.findViewById(R.id.list_image)
        val date: TextView? = itemView.findViewById(R.id.list_date)
        val delete: TextView? = itemView.findViewById(R.id.list_delete_button)

        fun bind(website: Website) {
            url?.text = website.url
            image?.setImageURI(website.uri)
            date?.text = website.dateTime.toString()
        }

        fun setOnClickListnerss(deleteListner: View.OnClickListener, load: View.OnClickListener) {
            url?.setOnClickListener(load)
            image?.setOnClickListener(load)
            delete?.setOnClickListener(deleteListner)
        }

    }

}