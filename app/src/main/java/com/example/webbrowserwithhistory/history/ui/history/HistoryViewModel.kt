package com.example.webbrowserwithhistory.history.ui.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.webbrowserwithhistory.common.Repo
import com.example.webbrowserwithhistory.common.Website
import javax.inject.Inject

class HistoryViewModel @Inject constructor(val repo: Repo) : ViewModel() {
   // Delete the website. Make sure to update the Live data.
    fun deleteWebsite(website: Website) {
        repo.removeWebsite(website)
        list.value = repo.getWebsites()
    }

    var list = MutableLiveData<List<Website>>()

    init {
         list.value = repo.getWebsites()
    }
}

