package com.example.webbrowserwithhistory.history

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.webbrowserwithhistory.R
import com.example.webbrowserwithhistory.history.ui.history.HistoryFragment

class HistoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.history_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, HistoryFragment.newInstance())
                .commitNow()
        }
    }

}
