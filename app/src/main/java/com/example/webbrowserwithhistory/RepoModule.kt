package com.example.webbrowserwithhistory

import com.example.webbrowserwithhistory.common.Repo
import com.example.webbrowserwithhistory.common.RepoImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {

    @Provides
    @Singleton
    fun provideRepo(Repo: RepoImpl): Repo {
        return Repo
    }
}