package com.example.webbrowserwithhistory

import android.app.Application

class MyApplication : Application() {
    private lateinit var appComponent: appComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerappComponent.builder().build()
    }

    fun getAppcomponent(): appComponent {
        return appComponent
    }


}