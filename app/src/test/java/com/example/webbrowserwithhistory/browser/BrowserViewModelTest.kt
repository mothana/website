package com.example.webbrowserwithhistory.browser

import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.example.webbrowserwithhistory.common.Repo
import com.example.webbrowserwithhistory.common.Website
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import java.util.*

class BrowserViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()


    @Test
    fun successfulUrlInput() {
        val model = BrowserViewModelImpl(MockRepo())
        model.updateUrl("https://www.google.com")
        model.getUrl().observeOnce { assertEquals("https://www.google.com", it) }
    }

    @Test
    fun malformedUrlInput() {

        val model = BrowserViewModelImpl(MockRepo())
        model.updateUrl("www.google.com")
        model.getUrl().observeOnce { assertEquals("https://www.google.com", it) }
    }

    @Test
    fun saveAWebsite() {
        val model = BrowserViewModelImpl(MockRepo())
        // have to use the same Mocked uri for the website check as , mocks will have a different mock id if you create a new one
        val mockedUri = Mockito.mock(Uri::class.java)
        model.saveBitmap(mockedUri, "www", 123)
        assertTrue(model.repo.getWebsites().isNotEmpty())
        assert(model.repo.getWebsites().contains(Website("www", Date(123), mockedUri)))
    }


    class MockRepo : Repo {
        var empty = true
        private lateinit var savedWebsite: Website
        override fun getWebsites(): List<Website> {
            return if (empty) emptyList() else
                listOf(savedWebsite)
        }

        override fun saveWebsite(website: Website) {
            empty = false
            savedWebsite = website
        }

        override fun removeWebsite(website: Website) {
            // Do nothing. Its not used by this viewmodel
        }


    }


    fun <T> LiveData<T>.observeOnce(onChangeHandler: (T) -> Unit) {
        val observer = OneTimeObserver(handler = onChangeHandler)
        observe(observer, observer)
    }


    class OneTimeObserver<T>(private val handler: (T) -> Unit) : Observer<T>, LifecycleOwner {
        private val lifecycle = LifecycleRegistry(this)

        init {
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        }

        override fun getLifecycle(): Lifecycle = lifecycle

        override fun onChanged(t: T) {
            handler(t)
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        }
    }


}

