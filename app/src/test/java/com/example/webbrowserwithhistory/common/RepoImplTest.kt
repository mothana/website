package com.example.webbrowserwithhistory.common

import android.net.Uri
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito
import java.util.*

class RepoImplTest {

    @Test
    fun saveWebsite() {
        val (repo, website) = setupRepoWithOneWebsite()
        assertTrue(repo.getWebsites().contains(website))
        assertTrue(repo.getWebsites().size == 1)
    }

    @Test
    fun deleteWebsite() {
        val (repo, website) = setupRepoWithOneWebsite()
        repo.removeWebsite(website)
        assertTrue(!repo.getWebsites().contains(website))
        assertTrue(repo.getWebsites().isEmpty())
    }

    @Test
    fun deleteNonExistentWebsite() {
        val (repo, website) = setupRepoWithOneWebsite()
        repo.removeWebsite(Website("wwww", Date(123), Mockito.mock(Uri::class.java)))
        assertTrue(repo.getWebsites().contains(website))
        assertTrue(repo.getWebsites().size == 1)
    }


    private fun setupRepoWithOneWebsite(): Pair<RepoImpl, Website> {
        val repo = RepoImpl()
        val mockedUri = Mockito.mock(Uri::class.java)
        val website = Website("www.google.com", Date(123), mockedUri)
        repo.saveWebsite(website)
        return Pair(repo, website)
    }


}