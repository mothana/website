package com.example.webbrowserwithhistory.history.ui.history

import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.example.webbrowserwithhistory.common.Repo
import com.example.webbrowserwithhistory.common.Website
import junit.framework.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import java.util.*

class HistoryViewModelTest {


    @get:Rule
    val rule = InstantTaskExecutorRule()


    @Test
    fun deleteWebsite() {

        // prepare the initial state
        val website = Website("www", Date(123), Mockito.mock(Uri::class.java))
        val mockRepo = MockRepo()
        mockRepo.setUpinitState(website)

        // start the model and check inital state
        val model = HistoryViewModel(mockRepo)
        model.list.observeOnce {
            assertTrue(it.isNotEmpty())
            assertTrue(it.contains(website))
        }

        // delete the website and check state
        model.deleteWebsite(website)
        model.list.observeOnce {
            assertTrue(it.isEmpty())
            assertTrue(!it.contains(website))
        }
    }


    class MockRepo : Repo {

        var empty = false
        private lateinit var savedWebsite: Website

        override fun getWebsites(): List<Website> {
            return if (empty) emptyList() else
                listOf(savedWebsite)
        }

        override fun saveWebsite(website: Website) {
        }

        override fun removeWebsite(website: Website) {
            empty = true
        }

        fun setUpinitState(website: Website) {
            savedWebsite = website
        }


    }

    fun <T> LiveData<T>.observeOnce(onChangeHandler: (T) -> Unit) {
        val observer = OneTimeObserver(handler = onChangeHandler)
        observe(observer, observer)
    }


    class OneTimeObserver<T>(private val handler: (T) -> Unit) : Observer<T>, LifecycleOwner {
        private val lifecycle = LifecycleRegistry(this)

        init {
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        }

        override fun getLifecycle(): Lifecycle = lifecycle

        override fun onChanged(t: T) {
            handler(t)
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        }
    }
}